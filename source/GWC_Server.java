import java.io.*;
import java.net.*;

public class GWC_Server {

    //initialize socket and input stream
    private Socket socket   = null;
    private ServerSocket server   = null;
    private DataInputStream in       =  null;

    // constructor with port
    private GWC_Server(int port)
    {
        // starts server and waits for a connection
        try
        {
            String fromclient;

            ServerSocket Server = new ServerSocket (port);

            System.out.println ("Server Waiting for client on port 5767");

            while(true)
            {
                Socket connected = Server.accept();
                System.out.println( " THE CLIENT"+" "+ connected.getInetAddress() +":"+connected.getPort()+" IS CONNECTED ");

                BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connected.getInputStream()));

                while (true)
                {
                    fromclient = inFromClient.readLine();
                    fromclient = fromclient.replaceAll("[(]", "");
                    fromclient = fromclient.replaceAll("[)]", "");
                    System.out.println( "RECIEVED: " + fromclient);
                    break;
                }
            }

        }
        catch(IOException i)
        {
            System.out.println(i);
        }
    }

    public static void main(String args[])
    {

        GWC_Server server = new GWC_Server(5767);
    }
}

