package javaapplication4;

import java.net.*;

import java.io.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;

/**
 *
 * @author ZeeYo
 */



public class JavaApplication4 {


    static class MyTask implements Runnable {
        boolean running = false;

        private void start() {
            running = true;
            new Thread(this).start();
            //Thread thread1 = new Thread(this, "Thread1");
            //Thread thread2 = new Thread("Thread2");
            //thread1.start();
            //thread2.start();
            //System.out.println("Thread names are following:");
            ///System.out.println(thread1.getName());
            //System.out.println(thread2.getName());
        }

        public void run() {

            while (running) {

                System.out.println("\ntesting\n");


                try {
                    Thread.sleep(1000);
                    System.out.println("Gateway Controller Socket started");

                    //Sets up socket using local hosts IP and set's Transmission Control Protocol (Let's two machines communicate) and set's port to #6191
                    Socket soc = new Socket("localhost", 6191);

                    System.out.println("GWD is connected to GW!");

                    //Output stream, codes data
                    //Output stream let's us send data to the server
                    DataOutputStream out = new DataOutputStream(new BufferedOutputStream(soc.getOutputStream()));
                    //Input stream, lets read data from server
                    //BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
                    DataInputStream in = new DataInputStream(soc.getInputStream());

                    //Sends request to server
                    out.writeUTF("GWC is requesting daily diagnostics");
                    out.flush();
                    System.out.println("Request for heartbeat has been sent");

                    //Reads response from server
                    //String str = in.readUTF();
                    Object obj = new JSONParser().parse(in.readUTF());
                    System.out.println("HEARTBEAT FROM GWD: " + obj);
                    JSONObject jo = (JSONObject) obj;
                    int id = ((Long) jo.get("Gateway ID")).intValue();
                    String Date = (String) jo.get("Date:");
                    String CPU_Integer = (String) jo.get("CPU Integer Math Test Result:");
                    String Battery = (String) jo.get("Battery:");
                    String Storage_Diagnostics = (String) jo.get("Storage Diagnostics Test Result:");
                    String Longitude = (String) jo.get("Longitude:");
                    String Timestamp = (String) jo.get("Timestamp:");
                    String Memory_Diagnostics = (String) jo.get("Memory Diagnostics Test Result:");
                    String CPU_Floating = (String) jo.get("CPU Floating Point Test Result:");
                    String Latitude = (String) jo.get("Latitude:");
                    float Time_taken_I = ((Double) jo.get("Time taken to complete CPU Integer Math :")).floatValue();
                    int Temperature = ((Long) jo.get("Temperature:")).intValue();
                    float Time_taken_F = ((Double) jo.get("Time taken to complete CPU Floating Point Test:")).floatValue();
                    //System.out.println(Date+CPU_Integer+Battery+Storage_Diagnostics+Longitude+Latitude+Timestamp+Memory_Diagnostics);
                    //String CPU_Integer_T=

                    String url = "https://dreamteam13.softwareengineeringii.com/heartbeatsapi/fulldiag?gw_id=%d" + id + "&timestamp=" + Timestamp + "&lat=" + Latitude + "&long=" + Longitude + "&battery_test_result=" + Battery + "&battery_test_flag= T&weather_test_result= 20 &weather_test_flag= F&memory_test_result=" + Memory_Diagnostics + "&memory_test_flag=T&storage_test_result=" + Storage_Diagnostics + "&storage_test_flag=T&CPUintegermap_test_result=" + CPU_Integer + "&CPUintegermap_test_flag=T&CPUfloatingpoint_test_result=" + CPU_Floating + "&CPUfloatingpoint_test_flag=T&CPUprimenumber_test_result=30&CPUprimenumber_test_flag= T";

                    HttpURLConnection httpClient =
                            (HttpURLConnection) new URL(url).openConnection();

                    // optional default is GET
                    httpClient.setRequestMethod("GET");

                    //add request header
                    httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");

                    int responseCode = httpClient.getResponseCode();
                    System.out.println("\nSending 'GET' request to URL : " + url);
                    System.out.println("Response Code : " + responseCode);

                    try (BufferedReader in2 = new BufferedReader(
                            new InputStreamReader(httpClient.getInputStream()))) {

                        StringBuilder response = new StringBuilder();
                        String line;

                        while ((line = in2.readLine()) != null) {
                            response.append(line);
                        }

                        //print result
                        System.out.println(response.toString());

                    }
                    //Closes inputstream
                    in.close();
                    //Closes outputstream
                    out.close();
                    //Closes Socket
                    soc.close();

                    System.out.println("Connection with GWD has been closed");

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }
    }


    public static void main(String[] args) {

        // TODO code application logic here

        new MyTask().start();

    }

}